import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_OpenBrowser'('https://www.krungsrimarket.com/CalculateLoan', 'Open Web krungsri Calculate Loan')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_Click'(findTestObject('Krungsrimarket/Calculate conditional credits/Page_/Calculate conditional credits TAB'), 
    'Click Calculate conditional credits TAB')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_SetText'(findTestObject('Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3CarPrice'), 
    'Input Car price', '500000')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_SetText'(findTestObject('Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3DownPaymentPercent'), 
    'Input Down Payment Percent', '10')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_SetText'(findTestObject('Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3RateCutOff'), 
    'Input Rate Cut OFF', '5')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_Click'(findTestObject('Krungsrimarket/Calculate conditional credits/Page_/input__btnTab3CalculateLoan'), 
    'Click Calculate Loan Button')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_CheckMessage'(findTestObject('Krungsrimarket/Calculate conditional credits/Page_/Installment rate per 72 month'), 
    'Check Installment rate (baht / month) (including VAT) per 12 month', '42,132')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_CloseBrowser'(findTestData('TC001').getValue(1, 1))

