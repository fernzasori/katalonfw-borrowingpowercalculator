import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_OpenBrowser'(findTestData('TC01CalculatedFromCar Price').getValue(1, 
        1))

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_OpenBrowser'('https://www.krungsrimarket.com/CalculateLoan', 'Open Web krungsri Calculate Loan')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_SelectOptionByValue'(findTestObject('Krungsrimarket/Calculated from car price/select_CarMakeId'), 
    'Select CarMakeId', '5')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_SelectOptionByValue'(findTestObject('Krungsrimarket/Calculated from car price/select_CarTypeId'), 
    'Select CarTypeId', '1')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_SelectOptionByValue'(findTestObject('Krungsrimarket/Calculated from car price/select_CarModelId'), 
    'Select CarModelId', '44')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_SelectOptionByValue'(findTestObject('Krungsrimarket/Calculated from car price/select_year'), 
    'Select Year', '2019')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_SetText'(findTestObject('Krungsrimarket/Calculated from car price/input__txtTab1CarPrice'), 
    'Input Car Prince', '1,000,000')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_SetText'(findTestObject('Krungsrimarket/Calculated from car price/input__txtTab1DownPaymentPercent'), 
    'Input Down Payment Percent ', '10')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_Click'(findTestObject('Krungsrimarket/Calculated from car price/input__btnTab1CalculateLoan'), 
    'Click CalculatelLoan')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_CheckMessage'(findTestObject('Krungsrimarket/Calculated from car price/Installment rate per 72 M'), 
    'Check Installment rate per 12 month', '83,460')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_CloseBrowser'('https://www.krungsrimarket.com/CalculateLoan', 'Web krungsri Calculate Loan')

WebUI.verifyEqual(null, null)

