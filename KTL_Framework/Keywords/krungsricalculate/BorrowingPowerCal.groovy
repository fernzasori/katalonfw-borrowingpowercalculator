package krungsricalculate

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.extosoft.keyword.UtilityKeyword
import com.extosoft.keyword.WebKeyword
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class BorrowingPowerCal {

	def WebKeyword web = new WebKeyword()
	def UtilityKeyword util = new UtilityKeyword()

	@Keyword
	def void LunchWebKrungsriCalculator(){

		util.FW_TestStartStep("เปิดเวปกรุงศรีเครื่องมือคำนวณ")
		web.FW_OpenBrowser("https://www.krungsri.com/bank/th/Other/Calculator.html")
		//		web.FW_WaitForElementVisible(findTestObject('Krungsrimarket/Calculated from car price/h1'))
		//		web.FW_SetText22(findTestObject('Object Repository/Krungsrimarket/Calculated from car price/CarMakeIdValue'), findTestData('TC001').getValue(7, 1))
		//web.FW_CheckMessage(findTestObject('Object Repository/Krungsrimarket/Calculated from car price/Calculated from car price Tab'), "ตรวจสอบชื่อแถบ “คำนวณจากราคารถยนต์”","คำนวณจากราคารถยนต์")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}


	//ตรวจสอบแถบเครื่องมือคำนวณ

	@Keyword
	def void CheckCal(){

		util.FW_TestStartStep("ตรวจสอบแถบเครื่องมือคำนวณ")
		//web.FW_OpenBrowser("https://www.krungsri.com/bank/th/Other/Calculator.html")
		//		web.FW_WaitForElementVisible(findTestObject('Krungsrimarket/Calculated from car price/h1'))
		//		web.FW_SetText22(findTestObject('Object Repository/Krungsrimarket/Calculated from car price/CarMakeIdValue'), findTestData('TC001').getValue(7, 1))
		web.FW_CheckMessage(findTestObject('Object Repository/TC001/Page_ l/cal'), "เครื่องมือคำนวณ","เครื่องมือคำนวณ")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void Checkhome(){
		util.FW_TestStartStep("ตรวจสอบแถบเครื่องคำนวณสินเชื่อบ้าน")
		//web.FW_OpenBrowser("https://www.krungsri.com/bank/th/Other/Calculator.html")
		//		web.FW_WaitForElementVisible(findTestObject('Krungsrimarket/Calculated from car price/h1'))
		//		web.FW_SetText22(findTestObject('Object Repository/Krungsrimarket/Calculated from car price/CarMakeIdValue'), findTestData('TC001').getValue(7, 1))
		web.FW_CheckMessage(findTestObject('Object Repository/TC001/Page_ l/homecal'), "เครื่องคำนวณสินเชื่อบ้าน","เครื่องคำนวณสินเชื่อบ้าน")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}


	@Keyword
	def void ClickPowercal(){

		util.FW_TestStartStep("กดเลือกเมนูคำนวณความสามารถในการกู้")
		//		web.FW_SetText(findTestObject('Krungsrimarket/Calculated from car price/input__txtTab1CarPrice'), '10')
		web.FW_Click(findTestObject('Object Repository/TC001/Page_ l/Powercal'), "หน้าจอแสดงตารางคำนวณความสามารถในการกู้")
		//web.FW_CheckMessage(findTestObject('Krungsrimarket/Calculated from car price/Installment rate per 72 M'), "หน้าจอแสงอัตราผ่อนชำระ(บาท/เดือน)(รวม vat)ที่72งวดเป็นเงิน “86,938 บาท”", findTestData('TC001').getValue(10, 1))
		//		findTestObject('Krungsrimarket/Calculated from car price/Installment rate per 72 M')
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void InputMonthlyIncome(){

		util.FW_TestStartStep("กรอกรายได้ต่อเดือน")
		//		web.FW_SetText(findTestObject('Krungsrimarket/Calculated from monthly/Page_/input_ ( VAT)_txtTab2PeriodPayValue'), '10000')
		//		web.FW_SetText(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Catalog/input__login'), "หน้าจอแสดงค่างวดต่อเดือน “10,000 บาท”", '10000')
		web.FW_SetText(findTestObject('Object Repository/TC001/Page_-/Monthincome'), "หน้าจอแสดงรายได้ต่อเดือน  “100,000.00”", findTestData('TC001').getValue(2, 1))
		//web.FW_CheckMessageByJS('Object Repository/TC001/Page_-/afterinputMonth',"100000.00",findTestData('TC001').getValue(2, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void InputLiability(){
		util.FW_TestStartStep("กรอกภาระหนี้ต่อเดือน")
		//		web.FW_SetText(findTestObject('Krungsrimarket/Calculated from monthly/Page_/input_ ( VAT)_txtTab2PeriodPayValue'), '10000')
		//		web.FW_SetText(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Catalog/input__login'), "หน้าจอแสดงค่างวดต่อเดือน “10,000 บาท”", '10000')
		web.FW_SetText(findTestObject('Object Repository/TC001/Page_-/Liability'), "หน้าจอแสดงภาระหนี้ต่อเดือน “10,000.00”", findTestData('TC001').getValue(3, 1))
		//web.FW_CheckMessageByJS('Object Repository/TC001/Page_-/afterinputMonth',"100000.00",findTestData('TC001').getValue(2, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void InputLoanTerm(){
		util.FW_TestStartStep("กรอกระยะเวลาที่ขอกู้ (ปี)")
		//		web.FW_SetText(findTestObject('Krungsrimarket/Calculated from monthly/Page_/input_ ( VAT)_txtTab2PeriodPayValue'), '10000')
		//		web.FW_SetText(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Catalog/input__login'), "หน้าจอแสดงค่างวดต่อเดือน “10,000 บาท”", '10000')
		web.FW_SetText(findTestObject('Object Repository/TC001/Page_-/inputyear'), "หน้าจอแสดงระยะเวลาที่ขอกู้ (ปี)“10”", findTestData('TC001').getValue(4, 1))
		//web.FW_CheckMessageByJS('Object Repository/TC001/Page_-/afterinputMonth',"100000.00",findTestData('TC001').getValue(2, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void ClickCalcurate(){
		util.FW_TestStartStep("กดปุ่มเริ่มคำนวณ")
		//		web.FW_SetText(findTestObject('Krungsrimarket/Calculated from car price/input__txtTab1CarPrice'), '10')
		web.FW_Click(findTestObject('Object Repository/TC001/Page_-/clickcal'), "หน้าจอแสดงผลการคำนวณ")
		//web.FW_CheckMessage(findTestObject('Krungsrimarket/Calculated from car price/Installment rate per 72 M'), "หน้าจอแสงอัตราผ่อนชำระ(บาท/เดือน)(รวม vat)ที่72งวดเป็นเงิน “86,938 บาท”", findTestData('TC001').getValue(10, 1))
		//		findTestObject('Krungsrimarket/Calculated from car price/Installment rate per 72 M')
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void VerifyMaximum(){
		util.FW_TestStartStep("ตรวจสอบยอดเงินที่ท่านสามารถกู้ได้")
		//web.FW_OpenBrowser("https://www.krungsri.com/bank/th/Other/Calculator.html")
		//		web.FW_WaitForElementVisible(findTestObject('Krungsrimarket/Calculated from car price/h1'))
		//		web.FW_SetText22(findTestObject('Object Repository/Krungsrimarket/Calculated from car price/CarMakeIdValue'), findTestData('TC001').getValue(7, 1))
		web.FW_CheckMessage(findTestObject('Object Repository/TC001/Page_-/span_4716000'), "4,716,000",findTestData('TC001').getValue(5, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void VerifyRepayment(){
		util.FW_TestStartStep("ตรวจสอบยอดเงินผ่อนชำระต่อเดือน")
		//web.FW_OpenBrowser("https://www.krungsri.com/bank/th/Other/Calculator.html")
		//		web.FW_WaitForElementVisible(findTestObject('Krungsrimarket/Calculated from car price/h1'))
		//		web.FW_SetText22(findTestObject('Object Repository/Krungsrimarket/Calculated from car price/CarMakeIdValue'), findTestData('TC001').getValue(7, 1))
		web.FW_CheckMessage(findTestObject('Object Repository/TC001/Page_-/span_55000'), "55,000",findTestData('TC001').getValue(6, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword

	def void CloseWebKrungsriCalculator(){
		util.FW_TestStartStep("ปิดเวปกรุงศรีเครื่องมือคำนวณ")

		web.FW_CloseBrowser("https://www.krungsri.com/bank/th/Other/Calculator.html")

		util.FW_TestEndStep()
	}


}
